# Analysis and Design Info Systems

This is a repository for a paper on the "Analysis and Design of Information Systems" class.

This class is part of the undergraduate curriculum of the Information and Computer Engineering
department, [ICE](http://www.ice.uniwa.gr), of the University of West Attica, [UniWa](https://www.uniwa.gr)

## Pharmaceutical Storage Management

This paper is centered around the management of a storage area for pharmaceuticals
and various other items belonging to a hospital.
