#!/bin/sh
# License; GPLv3
# Script to write the paper in an organized manner.
INDIR="paper.tex"
OUTDIR="../out"
TEMPDIR="writing_room"
PROJ_ROOT="../"
RELEASE_NAME="Omada-44-Ergasia.pdf"
cp $INDIR writing_room/temp-document.tex
# Go through the usal procedure after customization of release name and dir
# The writing happens in the tempdir!
cd $TEMPDIR
xelatex --shell-escape temp-document.tex
cd ..
echo "\033[38;5;3m*\033[m XeLaTeX finished"
echo "\033[38;5;3m*\033[m Moving .out to other/"
mv $TEMPDIR/*.out $TEMPDIR/other/
echo "\033[38;5;3m*\033[m Moving logs to .logs/"
mv $TEMPDIR/*.log $TEMPDIR/logs/
#echo "\033[38;5;3m*\033[m Making the release name and copying at out/"
#cp $TEMPDIR/temp-document.pdf $OUTDIR/$RELEASE_NAME
echo "\033[38;5;3m*\033[m Prepending the cover and copying at out/"
pdfunite $PROJ_ROOT/src/cover.pdf $TEMPDIR/temp-document.pdf $OUTDIR/$RELEASE_NAME
echo "Script ended."
if [ "$1" = "--open-pdf" ]
then 
    okular $OUTDIR/$RELEASE_NAME &
    echo "Displaying pdf..."
fi
